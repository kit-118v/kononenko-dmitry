﻿using System;

namespace Kononenko
{
    public class Student
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime EnterDate { get; set; }
        public string GroupIndex { get; set; }
        public string Faculty { get; set; }
        public string Specialization { get; set; }
        public int Performance { get; set; }

        public Student()
        {
            LastName = "Kononenko";
            FirstName = "Dmitry";
            Patronymic = "Alekseevich";
            BirthDate = new DateTime(2001, 6, 3);
            EnterDate = new DateTime(2018, 9, 1);
            GroupIndex = "v";
            Faculty = "CIT";
            Specialization = "Computer engeneering";
            Performance = 60;
        }
        public Student(string newLastName, string newFirstName, string newPatronymic, DateTime newBirthDate,
            DateTime newEnterDate, string newGroupIndex, string newFaculty, string newSpecialization, int newPerformance)
        {
            LastName = newLastName;
            FirstName = newFirstName;
            Patronymic = newPatronymic;
            BirthDate = newBirthDate;
            EnterDate = newEnterDate;
            GroupIndex = newGroupIndex;
            Faculty = newFaculty;
            Specialization = newSpecialization;
            Performance = newPerformance;
        }

        public override bool Equals(object obj)
        {
            Student another = (Student)obj;
            return LastName.ToLower().Equals(another.LastName.ToLower()) &&
                FirstName.ToLower().Equals(another.FirstName.ToLower()) &&
                Patronymic.ToLower().Equals(another.Patronymic.ToLower()) &&
                BirthDate.Equals(another.BirthDate) &&
                EnterDate.Equals(another.EnterDate) &&
                GroupIndex.ToLower().Equals(another.GroupIndex.ToLower()) &&
                Faculty.ToLower().Equals(another.Faculty.ToLower()) &&
                Specialization.ToLower().Equals(another.Specialization.ToLower()) &&
                Performance == another.Performance;
        }

        public override string ToString()
        {

            return $"{LastName}|{FirstName}|{Patronymic}|{BirthDate.ToString("d")}|" +
                $"{EnterDate.ToString("d")}|{GroupIndex}|{Faculty}|{Specialization}|{Performance}"; ;
        }

    }
}
