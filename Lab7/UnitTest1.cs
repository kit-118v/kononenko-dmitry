using Kononenko;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KononenkoTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var studentTest = new StudentContainer();
            var student1 = new Student("Kononenko", "Dmytro", "Aleck", DateTime.Parse("2001-06-03"),
                DateTime.Parse("2018-09-01"), "v", "Kit", "Programmer", 100);
            var student2 = new Student("Zozuly", "Vlad", "Vladim", DateTime.Parse("2001-03-06"),
               DateTime.Parse("2018-09-01"), "v", "Kit", "Programmer", 100);
            var student3 = new Student("Mischenko", "Dmytro", "Kfdsjge", DateTime.Parse("2001-09-14"),
               DateTime.Parse("2018-09-01"), "v", "Kit", "Programmer", 100);
            var student4 = new Student("Kulyk", "Daniil", "Igorevich", DateTime.Parse("2000-07-20"),
               DateTime.Parse("2018-09-01"), "v", "Kit", "Programmer", 100);
            Assert.AreEqual(Calculations.GetAge(DateTime.Parse("03.03.2001")),19);
            Assert.AreEqual(Calculations.GetAge(DateTime.Parse("04.06.2002")), 18);
            Assert.AreEqual(Calculations.GetAge(DateTime.Parse("03.05.1990")), 30);
            Assert.AreEqual(Calculations.GetAge(DateTime.Parse("19.06.2001")), 19);
            Assert.AreEqual(Calculations.GetAge(DateTime.Parse("03.07.2001")), 19);
            Assert.AreEqual(Calculations.GetAge(DateTime.Parse("03.11.2001")), 19);
            Assert.AreEqual(Calculations.GetAge(DateTime.Parse("30.12.2001")), 18);
            Io.Download(studentTest);
            Assert.AreEqual(studentTest.Students[0],student1);
            Assert.AreEqual(studentTest.Students[1], student2);
            Assert.AreEqual(studentTest.Students[2], student3);
            Assert.AreEqual(studentTest.Students[3], student4);
        }
    }
}
